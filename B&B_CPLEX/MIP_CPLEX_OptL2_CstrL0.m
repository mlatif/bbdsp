function [solution] = MIP_CPLEX_OptL2_CstrL0(H,y,K,solveur,heuristic,debug)
    [N,Q]=size(H);
    I_Q = speye(Q);Z_NQ = zeros(N,Q);Z_QN = zeros(Q,N);Z_1N = zeros(1,N);Z_1Q = zeros(1,Q);
    sostype=[];sosind =[];soswt=[];
    if (Q<N)
        C = [H,Z_NQ];x0 = zeros(Q,1);b0 = (x0~=0);v0 = [x0;b0];
        ctype(1:Q) = 'C'; ctype(Q+1:2*Q)='B';
        Aineq = [ -I_Q,zeros(Q);I_Q, -I_Q; zeros(1,Q),ones(1,Q);ones(1,Q),zeros(1,Q)]; bineq = [ zeros(Q,1); zeros(Q,1);K;1];
        Aeq = [];  beq = []; lb = zeros(2*Q,1);ub = ones(2*Q,1);
    elseif(Q>=N)
        C = sparse([Z_NQ,Z_NQ,speye(N)]);  x0 = zeros(Q,1);b0 = (x0~=0);z0 = H*x0;v0 = [x0;b0;z0];
        ctype(1:Q) = 'C';ctype(Q+1:2*Q) = 'B';ctype(2*Q+1:2*Q+N) = 'C';
        lb = zeros(2*Q,1); lb = [lb; -inf(N,1)];
        ub = ones(2*Q,1); ub = [ub; +inf(N,1)];
        Aineq = [ -I_Q,zeros(Q),Z_QN;I_Q, -I_Q, Z_QN;zeros(1,Q),ones(1,Q),Z_1N;ones(1,Q),zeros(1,Q),Z_1N];
        bineq = [ zeros(Q,1);zeros(Q,1);K;1];
        Aeq = sparse([H, Z_NQ, -eye(N)]);  beq = zeros(N,1);
    end
    if(debug.chrono), tic; end
        [x,z,exitflag,residual,output] = cplexlsqmilp(C,y,Aineq,bineq,Aeq,beq,sostype,sosind,soswt,lb,ub,ctype,v0,solveur);
    if (debug.chrono), chronoFin = toc; else chronoFin = 0; end
    z_norm = 0.5*(z-y'*y);
    if (output.cplexstatus >= 100)
        solution.X = x(1:Q,1);solution.B = x(Q+1:2*Q,1);
        solution.Z = z_norm;
        solution.nbIter = output.iterations;
        if (output.cplexstatus <= 0), solution.fail = 1; else,   solution.fail = 0; end
        solution.S0 = find(x(1:Q)==0 | x(1:Q) < 10e-4);
        solution.S1 = find(x(1:Q) & x(1:Q) >= 10e-4);
        solution.xtruth = debug.xtruth;
        if (debug.chrono),  solution.chronoTicToc = chronoFin; else solution.chronoTicToc = 0; end; solution.timeRelax = output.time;
        S1_t = find(debug.xtruth);
        solution.nbCd = length(intersect(S1_t,solution.S1));solution.nbCnd = length(setdiff(S1_t,solution.S1));
        solution.err_l2 = solution.Z;
        b_exp = (solution.X ~= 0 & solution.X > 10e-4);  b_exp = b_exp(1:size(debug.xtruth,1),1);  b_truth = (debug.xtruth ~=0);
        solution.err_support = sum(abs(b_exp - b_truth));
        solution.SolveurOutput = output;  solution.residual = residual;
        solution.unitExitFlag = output.cplexstatus;solution.stopFlag = debug.stopFlag;solution.exitflag = exitflag;
        solution.unitResTime = [];solution.unitObjValue = [];solution.unitCompSol = [];solution.nbNode = [];solution.traceRoute = [];
        solution.cpt_Bo1 = debug.cpt_Bo1;solution.cpt_Bo2 = debug.cpt_Bo2;solution.cpt_Bo3 = debug.cpt_Bo3;solution.cpt_Br = debug.cpt_Br;
        solution.branchVarIdx = [];solution.noIterSol = debug.noIterSol;solution.timeCritReached = debug.timeCritReached;
    else
        solution = [];
    end
end
