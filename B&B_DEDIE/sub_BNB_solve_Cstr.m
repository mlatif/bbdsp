function [x,z,output,debug,exitflag,stopFlag] = sub_BNB_solve_Cstr(noIter,node,i_xLb,i_xUb,solveur,debug,heuristic,cdtList,modele)
   t_solve = 0;
   stopFlag = 0;
   x_out = [];
   z_out = [];
   z = 0;
   x = [];
   exitflag = 1; output = [];
   % STEP 1 :
   if (~isempty(debug.unitCompSol))
        localQ = size(debug.unitCompSol(:,:),2); %
   end
   % STEP 3 :
   [H,y,HtH,ytH,Aineq,bineq,Aeq,beq,x0,xLb,xUb,xstatus,local_dim,HP_HtH,HP_ytH,FCLS_H,FCLS_y] = sub_solve_genereModel(noIter,modele,i_xLb,i_xUb,heuristic,debug,cdtList);    % Les i_.. sont pour input (puisque l'on retourne également les xLb et xUb mais au format pour la resolution.
   heuristic.local_dim = local_dim;
   % STEP 4 :
   switch heuristic.solverUsed
       case 1       % CPLEX QP
           tic;
                [x_out,z_out,exitflag,output] = cplexqp(HtH,-ytH,Aineq,bineq,Aeq,beq,xLb,xUb,x0,solveur);
           t_solve = toc;
           z = z_out;
       case 2       % MATLAB QUADPROG
           tic;
                [x_out,z_out,exitflag,output] = quadprog(HtH,-ytH,Aineq,bineq,Aeq,beq,xLb,xUb,x0,solveur);
           t_solve = toc;
           z = z_out;
       case 3       % CPLEX LSQ
           tic;
                [x_out,z_out,~,exitflag,output] = cplexlsqlin(H,y,Aineq,bineq,Aeq,beq,xLb,xUb,x0,solveur);
           t_solve = toc;
           z = 0.5*(z_out-y'*y);
       case 4       % MATLAB LSQLIN
            tic;
                [x_out,z_out,~,exitflag,output] = lsqlin(H,y,Aineq,bineq,Aeq,beq,xLb,xUb,x0,solveur);
            t_solve = toc;
            z = z_out-0.5*(y'*y);
       case 5       % METHODE HOMOTOPIE POSITIVE ~ hybride
            if (mod(node,2)==0 || node == 0)
                debug.curIt = node;
                tic;
                    [x_HP,vecLambda] = sub_BNB_solMeth_HP(HP_HtH,HP_ytH,heuristic,debug);
                t_solve = toc;
                x_out = x_HP;
            elseif(mod(node,2)==1)
                tic;
                    [x_out,z_out,exitflag,output] = cplexqp(HtH,-ytH,Aineq,bineq,Aeq,beq,xLb,xUb,x0,solveur);
                t_solve = toc;
                z = z_out;
            end
       case 6   % METHODE FCLS
            tic;
                [x_out] = sub_BNB_solMeth_FCLS(FCLS_y,FCLS_H);
            t_solve = toc;
   end
   % STEP 5 :
   if (exitflag < 0)
       stopFlag = 1;
   end
   % STEP 6 :
   if (ismember(Inf,xUb) || ismember(-Inf,xLb))
       x_tmp = x_out(xstatus,:);
   else
       x_tmp = x_out;
   end
   % STEP 8 :
   if (~isempty(cdtList) && length(x_tmp) < localQ)
       x = zeros(localQ,1);
       x(cdtList) = x_tmp;
       [r,~,v] = find(x~=0);
   elseif(isempty(cdtList))
       x = x_tmp;
   end
   % STEP 9
   if (heuristic.solverUsed == 5  && (mod(node,2)==0 || node == 0))
        z = 1/2*(x)'*(modele.H)'*modele.H*x - modele.y'*modele.H*x;
    end
    if (heuristic.solverUsed == 6)
        z = 1/2*(x)'*(modele.H)'*modele.H*x - modele.y'*modele.H*x;
    end
   % STEP 10 :
   if (~stopFlag)
        debug.unitObjValue = [debug.unitObjValue;  double(z)];
        debug.unitCompSol = [debug.unitCompSol; x'];
        if (heuristic.solverUsed == 5 && (mod(node,2)==0 || node == 0))
            debug.lambdaVec = [debug.lambdaVec; vecLambda ];
        end
   end
   debug.noIterSol = [debug.noIterSol;noIter];
   debug.unitResTime = [debug.unitResTime; t_solve];
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [r_C,r_y,r_HtH,r_ytH,r_Aineq,r_bineq,r_Aeq,r_beq,r_x0,r_xLb,r_xUb,r_xstatus,r_local_dim,r_HP_HtH,r_HP_ytH,r_FCLS_H,r_FCLS_y] = sub_solve_genereModel(i,modele,xLb,xUb,heuristic,debug,cdtList)
    r_xstatus = []; r_C = []; r_HtH = []; r_y = []; r_Aineq = []; r_bineq = [];   r_ytH = [];
    r_Aeq = []; r_beq = []; r_x0 = [];  r_xLb = []; r_xUb = []; r_local_dim = [];
    r_HP_HtH = [];  r_HP_ytH = [];  r_FCLS_H = [];  r_FCLS_y = [];
    H = modele.H; y = modele.y;
    [N,Q] = size(H);
    r_local_dim.N = N;  r_local_dim.Q = Q;  r_local_dim.C = length(cdtList);
    Aineq = modele.Aineq;  bineq = modele.bineq;  Aeq = modele.Aeq; beq = modele.beq; x0 = modele.x0;
    switch heuristic.solverUsed
        case {1,2,3,4}, configModel_Prime = 1;  configModel_Hybrid = 0;
        case 5, configModel_Prime = 2;  configModel_Hybrid = 1;
        case 6, configModel_Prime = 3;  configModel_Hybrid = 0;
    end
    if (configModel_Prime == 1 && configModel_Hybrid == 0)
        if (isempty(cdtList))   % Affectation normale
            s_H = H;  s_Aineq = Aineq; s_bineq = bineq; s_Aeq = Aeq; s_bineq = bineq; s_x0 = x0; s_xLb = xLb; s_xUb = xUb;
        elseif(~isempty(cdtList) && heuristic.resCdd)
            s_H = H(:,cdtList); s_Aineq = Aineq(:,cdtList); s_bineq = bineq;  s_x0 = x0(cdtList,:); s_xLb = xLb(cdtList,:); s_xUb = xUb(cdtList,:);
        end
        [s_N,s_Q] = size(s_H);
        if(Q < N)
                r_C = s_H;  r_y = modele.y; r_Aineq = s_Aineq;  r_bineq = s_bineq;  r_Aeq = Aeq; r_beq = beq; r_x0 = s_x0; r_xLb = s_xLb; r_xUb = s_xUb; r_xstatus = ones(s_Q,1);
        elseif(Q>=N)
        % STEP 4.2
            Z_sNQ = zeros(s_N,s_Q); Z_s1N = zeros(1,s_N);
            r_C = sparse([Z_sNQ,speye(s_N)]); r_y = modele.y;     r_Aineq = [ones(1,s_Q),Z_s1N]; r_bineq = s_bineq;
            r_Aeq = sparse([s_H, -eye(s_N)]); r_beq = zeros(s_N,1); r_x0 = [s_x0; H*x0]; r_xLb = [s_xLb; -inf(s_N,1)]; r_xUb = [s_xUb; +inf(s_N,1)];
            r_xstatus = logical([ones(s_Q,1); zeros(s_N,1)]);
        end
        % STEP 5 :
        if (~heuristic.preCalcul)
            r_HtH = r_C'*r_C;
            r_ytH = y'*r_C;
        elseif(heuristic.preCalcul)
            if (N>Q)
                if (~heuristic.resCdd || i == 0)
                    r_HtH = modele.HtH; r_ytH = modele.ytH;
                elseif(heuristic.resCdd && i ~=0)
                    r_HtH = modele.HtH(cdtList,cdtList);  r_ytH = modele.ytH(:,cdtList);
                end
            else
                r_HtH = r_C'*r_C; r_ytH = [zeros(1,s_Q),modele.nz_ytH];
            end
        end
    elseif (configModel_Prime == 2 && configModel_Hybrid == 1)
        if (isempty(cdtList))
            s_HP_H = H;
        elseif(~isempty(cdtList) && heuristic.resCdd)
            s_HP_H = H(:,cdtList);
        end
        if (~heuristic.preCalcul || i == 0)
            r_HP_HtH = s_HP_H'*s_HP_H;    r_HP_ytH = y'*s_HP_H;
        elseif(heuristic.preCalcul && i ~=0)
            r_HP_HtH = modele.HP_HtH(cdtList,cdtList);  r_HP_ytH = modele.HP_ytH(:,cdtList);
        end
        if (isempty(cdtList))
            s_H = H;  s_Aineq = Aineq; s_bineq = bineq; s_Aeq = Aeq; s_bineq = bineq; s_x0 = x0; s_xLb = xLb; s_xUb = xUb;
        elseif(~isempty(cdtList) && heuristic.hybSolver.resCdd)
            s_H = H(:,cdtList); s_Aineq = Aineq(:,cdtList); s_bineq = bineq; s_x0 = x0(cdtList,:); s_xLb = xLb(cdtList,:); s_xUb = xUb(cdtList,:);
        end
        [s_N,s_Q] = size(s_H);
        if(Q < N)
            r_C = s_H;  r_y = modele.y; r_Aineq = s_Aineq;  r_bineq = s_bineq;
            r_Aeq = Aeq;  r_beq = beq;  r_x0 = s_x0;  r_xLb = s_xLb;  r_xUb = s_xUb;
            r_xstatus = ones(s_Q,1);
        elseif(Q>=N)
            Z_sNQ = zeros(s_N,s_Q); Z_s1N = zeros(1,s_N);
            r_C = sparse([Z_sNQ,speye(s_N)]); r_y = modele.y; r_Aineq = [ones(1,s_Q),Z_s1N];  r_bineq = s_bineq;
            r_Aeq = sparse([s_H, -eye(s_N)]); r_beq = zeros(s_N,1);   r_x0 = [s_x0; H*x0];  r_xLb = [s_xLb; -inf(s_N,1)]; r_xUb = [s_xUb; +inf(s_N,1)];
            r_xstatus = logical([ones(s_Q,1); zeros(s_N,1)]);
        end
        if (~heuristic.hybSolver.preCalcul)
            r_HtH = r_C'*r_C; r_ytH = y'*r_C;
        elseif(heuristic.hybSolver.preCalcul)
            if (N>Q)
                if (~heuristic.hybSolver.resCdd || i == 0)
                    r_HtH = modele.HtH;   r_ytH = modele.ytH;
                elseif(heuristic.hybSolver.resCdd && i ~=0)
                    r_HtH = modele.HtH(cdtList,cdtList);  r_ytH = modele.ytH(:,cdtList);
                end
            else
                r_HtH = r_C'*r_C; r_ytH = [zeros(1,s_Q),modele.nz_ytH];
            end
       end
       elseif (configModel_Prime == 3 && configModel_Hybrid == 0)
            if (isempty(cdtList))
                r_FCLS_H = H;
            elseif(~isempty(cdtList) && heuristic.resCdd)
                r_FCLS_H = H(:,cdtList);
            end
            r_FCLS_y = y;
      end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [C] = sub_BNB_solMeth_FCLS(X,A)
    [Nf,Ns] = size(A);
    [Nf,Np] = size(X) ;
    One     = ones(Ns,1);
    tol   = 1e-9;
    %
    for np=1:Np
        e          = 1/10/max(X(:,np));
        eA         = e*A;
        E          = [One' ; eA];
        EtE        = E'*E;
        iEtE       = inv(EtE);
        iEtEOne    = iEtE*One;
        sumiEtEOne = sum(iEtEOne);
        weights    = diag(iEtE);
        er         = e*X(:,np);
        f          = [1;er];
        Etf        = E'*f;

        %%%% THIS IS lamdiv2
        ls      = iEtE*Etf;
        lamdiv2 = -(1-(ls'*One))/sumiEtEOne;
        x2      = ls - lamdiv2*iEtEOne;
        x2old   = x2;
        iter_on_whole = 0;
        %
        if (any(x2<-tol))
            Z = zeros(Nf,1);
            iter=0;
            while (any(x2<-tol))
                Z(x2<-tol)=1;
                zz = find(Z);
                x2 = x2old; % Reset x2
                L  = iEtE(zz,zz);
                ab = size(zz);
                lastrow = ab(1)+1;
                lastcol = lastrow;
                L(lastrow,1:ab(1)) = (iEtE(:,zz)'*One)';
                L(1:ab(1),lastcol) = iEtEOne(zz);
                L(lastrow,lastcol) = sumiEtEOne;
                xerow = x2(zz);
                xerow(lastrow,1) = 0;
                lagra = L\xerow;
                while (any(lagra(1:ab(1))>1e-6))  % Reset lagrange multipliers - PATCH SEB
                    maxneg = weights(zz).*lagra(1:ab(1));
                    [yz,iz] = max(maxneg); % Remove the most positive
                    Z(zz(iz)) = 0;
                    zz = find(Z);  % Will always be at least one (prove)
                    L  = iEtE(zz,zz);
                    ab = size(zz);
                    lastrow = ab(1)+1;
                    lastcol = lastrow;
                    L(lastrow,1:ab(1)) = (iEtE(:,zz)'*One)';
                    L(1:ab(1),lastcol) = iEtEOne(zz);
                    L(lastrow,lastcol) = sumiEtEOne;
                    xerow = x2(zz);
                    xerow(lastrow,1)=0;
                    lagra = L\xerow;
                end
                if ~isempty(zz)
                    x2 = x2 - iEtE(:,zz)*lagra(1:ab(1))-lagra(lastrow)*iEtEOne;
                end
                iter=iter+1;
                if iter>L, break, end
            end
            iter_on_whole = iter_on_whole+1;
            if (iter_on_whole>10*L), break; end
        end
        C(:,np) = x2;
      end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [x,vecLambda] = sub_BNB_solMeth_HP(HtH,ytH,heuristic,debug)
    tau = heuristic.HPtau;  nbIter = heuristic.HPnbIter;
    vecLambda = zeros(1,nbIter);  vecCriter = zeros(1,nbIter);
    if (heuristic.local_dim.C == 0), Q = heuristic.local_dim.Q; else Q = heuristic.local_dim.C; end
    % STEP 1 :
    [lambda,I] = max(abs(ytH));
    x = zeros(Q,1);
    for it=1:nbIter
        % STEP 2 :
        J= 1:Q; J(I) = [];
        % STEP 3 :
        c = ytH' - HtH(:,I)*x(I);
        % STEP 4 :
        d = zeros(Q,1); d(I) = HtH(I,I) \ sign(c(I));
        % STEP 5 :
        v = HtH(J,I)*d(I);
        % STEP 6 :
        w = ( lambda-c(J) ) ./ ( 1 - v ); gamma1 = min(w(w> 1e-10));
        if not(isempty(gamma1)), i1 = J( w==gamma1 ); end
        %%%%%%%%%%%%%
        % STEP 7 :
        w = -x(I)./d(I);  gamma3 = min(w(w> 1e-10));
        if not(isempty(gamma3)), i3 = I( w==gamma3 ); end
        % STEP 8 :
        gamma = min([gamma1 gamma3]);
        % STEP 9 :
        if isempty(gamma) break;  end
        % STEP 10 :
        x(I) = x(I) + gamma*d(I);
        % STEP 11 :
        nx = norm(x(I),1);
        if (nx >= tau)
            if nx > tau, sn=sign(x(I) - gamma*d(I)/2); gm= (nx- tau)/ (d(I)' * sn);  x(I)= x(I) - gm*d(I);  end
            break;
        end
        % STEP 12 :
        lambda = lambda - gamma;
        if (lambda < 10e-8),   break;  end
        % STEP 13 :
        vecLambda(1,it) = lambda; vecCriter(1,it) =  x'*HtH*x - 2*ytH*x;
        % STEP 14 :
        if gamma==gamma1
            I = [I i1];
        elseif gamma==gamma3
            I(I==i3) = [];  x(i3) = 0;
            idxNeg = find(x<0); x(idxNeg) = 0;
        end
    end
    idxNeg = find(x<0); x(idxNeg) = 0;
end
