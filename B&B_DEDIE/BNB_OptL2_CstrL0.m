function [solution] = BNB_OptL2_CstrL0(modele,params,solveur,heuristic,debug)
   baseTime = heuristic.BBmaxTime;
   H = modele.H;
   y = modele.y;
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   [N,Q] = size(H);
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   if (debug.chrono), tic; end
   % STEP 0
   x0=params.v0;
   xstatus=params.xstatus;
   errMsgG = "";
   exitflagP=0;
   nbNode = 0;
   fail = Inf;
   z_incumbent = 0; x_incumbent = [];
   timeCritReached = 0;
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   % STEP 1
   [stopFlag,xstatus,bLb,bUb,modeleC] = sub_BNB_input_Checking(x0,xstatus,modele);
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   % STEP 2
   if (~stopFlag)
       lx = size(x0,1);
       x0(x0<bLb)=bLb(x0<bLb);
       x0(x0>bUb)=bUb(x0>bUb);
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   % STEP 2.1 :
       [z_incumbent,x_incumbent] = sub_BNB_fixGlobBsup(modeleC,x0);
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   % STEP 2.2 :
       if(heuristic.choice_Separation == 1)
            I = params.K+1;
       elseif(heuristic.choice_Separation == 2)
            I = length(x0)+1;
       end
       stackbLb=zeros(lx,I); stackbLb(:,1)=bLb;
       stackbUb=zeros(lx,I); stackbUb(:,1)=bUb;
       stackdepth=zeros(1,I);
       stacksize=1;
       stackCptDepthK = zeros(1,I);
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   % STEP 2.3 :
       timeRelax = 0;
       cardCpt = 0;
       isRoot = 1;
       isLeaf = 0;
       isExplore = 0;
       nbIter = 0;
       iSepStack = zeros(1,I);
       nbCdt = [];
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       while(stacksize > 0 && timeRelax < heuristic.BBmaxTime && debug.forceStop~=1 && ~stopFlag)
   % STEP 2.3.0 :
            debug.traceRouteNoIter = [debug.traceRouteNoIter;nbIter];
            nbNode = nbNode + 1;  debug.nIter = debug.nIter + 1; cdtList = []; unitExitFlag = 0;
   % STEP 2.3.1 :
            bLb=stackbLb(:,stacksize);
            bUb=stackbUb(:,stacksize);
            node=stackdepth(1,stacksize);
            cptKDepth = stackCptDepthK(1,stacksize);
            stackCptDepthK(1,stacksize) = 0; stackDepthPrint = stackdepth; stackdepth(1,stacksize) = 0;
            if (stacksize <= 1), idxSep = 0; else idxSep = iSepStack(1,stacksize-1); end
            iSepStack(1,stacksize)=0;
            stacksize=stacksize-1;
            separation = 1;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % STEP 2.3.2.1 :
            if(heuristic.choice_Separation == 1 && mod(node,2) == 0 && ~isRoot), isExplore = 1; end
            if (cptKDepth == params.K), isLeaf = 1; end
    % STEP 2.3.2.3 :
            debug.traceRouteNodes = [debug.traceRouteNodes; node,cptKDepth];
    % STEP 2.3.2.4 :
            if(isRoot)
                debug.traceRouteTypeNodes = [debug.traceRouteTypeNodes;0];
            elseif(isLeaf)
                debug.traceRouteTypeNodes = [debug.traceRouteTypeNodes;1];
            elseif(isExplore)
                debug.traceRouteTypeNodes = [debug.traceRouteTypeNodes;2];
            else
                debug.traceRouteTypeNodes = [debug.traceRouteTypeNodes;3];
            end
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
     % STEP 2.3.3 :
            typeNodeInfo ="";
            resol = 0;
     % STEP 2.3.3.a
            if(isRoot)
                typeNodeInfo = "Root";
                [xP,zP,output,debug,exitflagP,stopFlag] = sub_BNB_solve_Cstr(nbIter,node,bLb,bUb,solveur,debug,heuristic,[],modeleC);
                separation = 1; resol = 1;
    % STEP 2.3.3.b
            elseif(isLeaf)
                typeNodeInfo = "Leaf";
                bLbP = zeros(length(xP),1); bUbP = bLb;
                if(heuristic.resCdd), [cdtList,~] = getCandidat(typeNodeInfo,bLbP,bUbP,debug,heuristic,xstatus); end
                [xP,zP,output,debug,exitflagP,stopFlag] = sub_BNB_solve_Cstr(nbIter,node,bLbP,bUbP,solveur,debug,heuristic,cdtList,modeleC);
                separation = 0; resol = 1;
     % STEP 2.3.3.c
            elseif(isExplore)
                typeNodeInfo= "Explore";
                bLbP = zeros(length(xP),1);
                if (heuristic.resCdd), [cdtList,~] = getCandidat(typeNodeInfo,bLbP,bUb,debug,heuristic,xstatus);end
                [xP,zP,output,debug,exitflagP,stopFlag] = sub_BNB_solve_Cstr(nbIter,node,bLbP,bUb,solveur,debug,heuristic,cdtList,modeleC);
                resol = 1; separation = 1;
            end % if(isRoot), elseif(isLeaf), elseif(isExplore)
            %%%
     % STEP 2.3.3.d
            if (exitflagP <0)
                fail = fail+1;
            end
     % STEP 2.3.3.g
            debug.traceRouteNoIterResol = [debug.traceRouteNoIterResol; resol];
     % STEP 2.3.3.h
            tR_Binf = 0;
            if (isempty(debug.binf) && resol)
                debug.binf = [debug.binf;zP];
                tR_Binf = zP;
            elseif(resol)
                elt = min(min(debug.binf),zP);
                debug.binf = [debug.binf;elt];
                tR_Binf = elt;
            elseif(~resol)
                elt = min(debug.binf);
                debug.binf = [debug.binf;elt];
                tR_Binf = elt;
            end   % if (isempty(debug.binf) && resol) elseif ...
            debug.traceRouteBinf = [debug.traceRouteBinf;tR_Binf];
      % STEP 2.3.3.i
            if (resol), unitExitFlag = exitflagP;end
            debug.unitExitFlag = [debug.unitExitFlag; unitExitFlag];
    % STEP 2.3.3.j
           nbCdt = [nbCdt; length(cdtList)];
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
     % STEP 2.3.4
             V = find(bLb~=bUb);
     % STEP 2.3.5 :
            tR_boundOp = 0;
            if((~isRoot && isLeaf)||(~isRoot && isExplore))
     % STEP 2.3.5.a :
                if (exitflagP <= 0)
                    separation = 0;
                    tR_boundOp = 1;
     % STEP 2.3.5.b :
                elseif(exitflagP > 0 && zP >= z_incumbent)
                    separation = 0;
                    tR_boundOp = 2;
     % STEP 2.3.5.c :
                elseif(exitflagP >0 && all(abs(round(xP(V))-xP(V))<heuristic.integTolerance))
                    separation = 0;
                    z_incumbent = zP;
                    x_incumbent = xP;
                    tR_boundOp = 3;
                end
                %%%%%%
            end %if(~isRoot)
     % STEP 2.3.5.d :
            debug.traceRouteBoundOps = [debug.traceRouteBoundOps;tR_boundOp];
     % STEP 2.3.5.e :
            debug.bsup = [debug.bsup;z_incumbent];  debug.traceRouteBsup = [debug.traceRouteBsup;z_incumbent];
     % STEP 2.3.5.f :
            gap = 100 - (100*zP)/z_incumbent; debug.traceRouteGap = [debug.traceRouteGap;gap];
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
     % STEP 2.3.6 :
            tR_branchIdx = 0;
            tracker_isep = 0;
            if (separation == 1 && ~isempty(V) && ~isLeaf && ~stopFlag)
                if(isRoot), isRoot = 0;end
                    [isep,heuristic] = sub_BNB_select_var(heuristic,V,xP,debug,xstatus);
                    tR_branchIdx = isep;
     % STEP 2.3.7 :
                    branch=1;
                    domain=[bLb(isep) bUb(isep)];
                    while(branch==1 && ~debug.forceStop)
     % STEP 2.3.7.a
                        [domainG,domainD] = sub_BNB_select_branch(xP,isep,domain,heuristic.choice_Separation);
     % STEP 2.3.7.b :
                        stacksize=stacksize+1;
                        %iSepStack(1,stacksize) = isep;
                        stackbLb(:,stacksize)=bLb;
                        stackbLb(isep,stacksize)=domainD(1);
                        stackbUb(:,stacksize)=bUb;
                        stackbUb(isep,stacksize)=domainD(2);
                        stackdepth(1,stacksize)=2*node+2;
                        stackCptDepthK(1,stacksize) = cptKDepth;
     % STEP 2.3.7.c :
                        if(domainG(1)==domainG(2))
                            stacksize=stacksize+1;
                            stackbLb(:,stacksize)=bLb;
                            stackbLb(isep,stacksize)=domainG(1);
                            stackbUb(:,stacksize)=bUb;
                            stackbUb(isep,stacksize)=domainG(2);
                            stackdepth(1,stacksize)=2*node+1;
                            stackCptDepthK(1,stacksize) = cptKDepth+1;
                            branch = 0;
                        else
                            domain=domainG;
                            branch=1;
                            errMsgG='Probleme binaire: branch=1.'; return;
                        end
                    end
            end
      % STEP 2.3.7.d :
            debug.traceRouteBranchIdx = [debug.traceRouteBranchIdx;tR_branchIdx];
      % STEP 2.3.8 :
            if (isLeaf), isLeaf = 0;end
            if (isExplore), isExplore = 0; end
            nbIter = nbIter + 1;
      % STEP 2.3.9 :
            if (resol), timeRelax = timeRelax + debug.unitResTime(length(debug.unitResTime));end
      % STEP 2.3.10 :
            if (debug.unitExitFlag(length(debug.unitExitFlag)) <0)
                if (fail == Inf), fail = 0; end
                fail = fail + 1;
            end
      % STEP 2.3.11 :
       end
       if (debug.chrono), chronoFin = toc; else, chronoFin = 0; end
       if (timeRelax >= heuristic.BBmaxTime), timeCritReached = 1; end
   end % if (~stopFlag)
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   % STEP 3 :
   % STEP 3.1
   solution.X = x_incumbent;  solution.Z = z_incumbent;
   if (params.validBrIdx(2) ~= length(x0))
        solution.B = [];
   else
        solution.B = x0(params.validBrIdx(1):params.validBrIdx(2));
   end
   solution.nbNode = nbNode;
   if (fail == Inf)
        solution.fail = 0;
   else
        solution.fail = fail;
   end
   solution.ninst = debug.ninst;
   solution.S0 = find(x_incumbent==0 & x_incumbent <= 10e-4);
   solution.S1 = find(x_incumbent~=0 & x_incumbent > 10e-4 & xstatus ==1);
   solution.xtruth = debug.xtruth;
   solution.unitResTime = debug.unitResTime;
   solution.unitObjValue = debug.unitObjValue;
   solution.unitCompSol = debug.unitCompSol;
   solution.timeRelax = timeRelax;
   solution.timeCritReached = timeCritReached;
   solution.nbIter = nbIter;
   % STEP 3.2
   S1_t = find(debug.xtruth);
   solution.nbCd = length(intersect(S1_t,solution.S1));
   solution.nbCnd = length(setdiff(S1_t,solution.S1));
   solution.err_l2 = solution.Z;
   % STEP 3.3
   b_exp = (solution.X ~= 0 & solution.X > 10e-4);
   b_exp = b_exp(1:size(debug.xtruth,1),1);
   b_truth = (debug.xtruth ~=0);
   solution.err_support = sum(abs(b_exp - b_truth));
   % STEP 3.4
   solution.exitFlag = debug.exitFlag;
   solution.unitExitFlag = debug.unitExitFlag;
   solution.SolveurOutput = debug.SolveurOutput;
   solution.residual = debug.residual;
   % STEP 3.5
   solution.traceRoute = [debug.traceRouteNoIter,debug.traceRouteNodes,debug.traceRouteTypeNodes,debug.traceRouteNoIterResol,debug.traceRouteBoundOps,debug.traceRouteBranchIdx,debug.traceRouteBinf,debug.traceRouteBsup,debug.traceRouteGap,debug.unitExitFlag,nbCdt];
   % STEP 3.6
   solution.cpt_Bo1 = length(find(solution.traceRoute(:,6)==1));
   solution.cpt_Bo2 = length(find(solution.traceRoute(:,6)==2));
   solution.cpt_Bo3 = length(find(solution.traceRoute(:,6)==3));
   solution.cpt_Br = length(find(solution.traceRoute(:,7)));
   solution.branchVarIdx = nonzeros(solution.traceRoute(:,7)');
   % STEP 3.7
   solution.noIterSol = debug.noIterSol;         % Ça me saoule mais je suis obligé de le garder pour  avoir l'évolution des bornes.
   solution.stopFlag = stopFlag;
   if (debug.chrono)
       solution.chronoTicToc = chronoFin;
   else
       solution.chronoTicToc = 0;
   end
   solution.lambdaVec = debug.lambdaVec;
   solution.HP_detectNegX = debug.detectNegX;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [z_inc, x_inc] = sub_BNB_fixGlobBsup(modeleC,x0)
    lx = size(x0,1);
    z_inc = Inf;
    x_inc = Inf*ones(lx,1);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [stopFlag,xstatus,xLb,xUb, modeleC] = sub_BNB_input_Checking(x0,xtype,modele)
    xl = modele.lb;xu = modele.ub;ai = modele.Aineq;bi = modele.bineq;ae = modele.Aeq;be = modele.beq;
    errMsgIC = "_"; warMsgIc = "_"; stopFlag = 0;
    xstatus = []; xLb = [];xUb =[]; Ai = []; Ae=[]; Bi = []; Be=[];
    %%%%%%%%%%%%%%%%
    if (isempty(x0))
        errMsgIC='Case 1 : No x0 found.'; stopFlag = 1; return;
    elseif ~isnumeric(x0) || ~isreal(x0) || size(x0,2)>1
       errMsgIC='Case 2 : x0 must be a real column vector.'; stopFlag = 1; return;
    end
    xstatus=zeros(size(x0));
    if (~isempty(xtype))
       if (isnumeric(xtype) && isreal(xtype) && all(size(xtype)<=size(x0)))
            if (all(xtype==round(xtype) & all(0<=xtype & xtype<=2)))
                xstatus(1:size(xtype))=xtype;
            else
                errMsgIC='Case 3 : xstatus must consist of the integers 0,1 en 2.';stopFlag = 1; return;
            end
       else
            errMsgIC='Case 4 : xstatus must be a real column vector the same size as x0.';stopFlag = 1; return;
       end
    end
    xLb=zeros(size(x0));
    xLb(xstatus==0) =-inf;
    xLb(xstatus==2) =-inf;
    if  (~isempty(xl))
       if (isnumeric(xl) && isreal(xl) && all(size(xl)==size(x0)))  % - Modif Gwenael | Original Ramzi => if (isnumeric(xl) && isreal(xl) && all(size(xl)<=size(x0)))
          xLb(1:size(xl,1))=xl;
       else
           errMsgIC='Case 5 : xLb must be a real column vector the same size as x0.'; stopFlag = 1; return;
       end
    end
    if (any(x0<xLb))
       errMsgIC='Case 6 : x0 must be in the range xLb <= x0.'; stopFlag =1;return;
    elseif (any(xstatus==1 & (xLb ~= 1 & xLb ~= 0)))    % - Modif Gwenael | Original Ramzi => any(xstatus==1 & (~isfinite(xLb) | xLb~=round(xLb)))
       % "les bornes inferieures des b_i doivent être entieres". Comme ce sont des binaires, on pourrait directement mettre une contrainte sur la borne inf en 0-1
       errMsgIC='Case 7 : xLb(i) must be an integer if x(i) is an integer variable.';stopFlag =1; return;
    end
    xLb(xstatus==2)=x0(xstatus==2);
    xUb=ones(size(x0));
    xUb(xstatus==0) = inf;
    xUb(xstatus==2) = inf;
    if (~isempty(xu))
       if(isnumeric(xu) && isreal(xu) && all(size(xu)==size(x0))) % - Modif Gwenael | Original Ramzi => % if(isnumeric(xu) && isreal(xu) && all(size(xu)<=size(x0)))
          xUb(1:size(xu,1))=xu;
       else
           errMsgIC='Case 8 : xUb must be a real column vector the same size as x0.'; stopFlag = 1; return;
       end
    end
    if (any(x0>xUb))
        errMsgIC='Case 9 : x0 must be in the range x0 <=xUb.';stopFlag = 1; return;
    elseif (any(xstatus==1 & (~isfinite(xUb) | xUb~=round(xUb))))
       errMsgIC='Case 10 : xUb(i) must be an integer if x(i) is an integer variabale.'; stopFlag =1; return;
    end
    xUb(xstatus ==2) =x0(xstatus==2);
    if (~isempty(ai))
       if (isnumeric(ai) && isreal(ai) && size(ai,2)==size(x0,1))
           Ai=ai;
       else
           errMsgIC='Case 11 : Matrix Ai not correct.'; stopFlag = 1;return;
       end
    end
    if (~isempty(bi))
       if (isnumeric(bi) && isreal(bi) && all(size(bi)==[size(Ai,1) 1]))
           Bi=bi;
       else
           errMsgIC='Case 12 : Column vector Bi not correct.';stopFlag = 1; return;
       end
    end
    if (isempty(Bi) && ~isempty(Ai))
        Bi=zeros(size(Ai,1),1);
        warMsgIc="Warning : Column vector Bi is empty and matrix Ai is not.";
    end
    if (~isempty(ae))
       if (isnumeric(ae) && isreal(ae) && size(ae,2)==size(x0,1))
           Ae=ae;
       else
           errMsgIC='Case 13 : Matrix Ae not correct.'; stopFlag = 1;return;
       end
    end
    if (~isempty(be))
       if (isnumeric(be) && isreal(be) && all(size(be)==[size(Ae,1) 1]))
           Be=be;
       else
           errMsgIC='Case 14: Column vector Be not correct.'; stopFlag = 1;return;
       end
    end
    %if(verbose), fprintf(">> Input_Checking\n     > errMsg = %s\n     > warMsg = %s\n     > stopFlag = %d \n\n",errMsgIC,warMsgIc,stopFlag); end
    modeleC.H = modele.H; modeleC.y = modele.y; modeleC.Aineq = Ai; modeleC.bineq = Bi;
    modeleC.Aeq = Ae ;  modeleC.beq = Be; modeleC.x0 = x0;
    [N,Q] = size(modele.H);
    if (N>Q)
        modeleC.HtH = modele.H'*modele.H; modeleC.ytH = modele.y'*modele.H; modeleC.nz_ytH = [];
    else
        C = sparse([zeros(N,Q),speye(N)]); modeleC.HtH = []; modeleC.ytH = modele.y'*C; modeleC.nz_ytH = modeleC.ytH(:,find(modele.y'*C));
    end

    modeleC.HP_HtH = modele.H'*modele.H; modeleC.HP_ytH = modele.y'*modele.H;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [cdtList,excList] = getCandidat(typeNode,lb,ub,debug,heuristic,xstatus)
    cdtList = find(ub~=0 & ub ~= +Inf & lb ~= -Inf);
    if(heuristic.withBi)
        tmp_ub = ((length(ub)/2):length(ub));
    else
        tmp_ub = (1:length(ub));
    end
    excList = [];
    excList = setdiff(tmp_ub,cdtList)';
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ domainG,domainD ] = sub_BNB_select_branch(x,isep,domain,choice)
    xboundary=(domain(1)+domain(2))/2;
    switch choice
        case 1
            if x(isep)==0
                domainG=[domain(1) floor(xboundary)];
                domainD=[floor(xboundary+1) domain(2)];
            else
                domainG=[floor(xboundary+1) domain(2)];
                domainD=[domain(1) floor(xboundary)];
            end
        case 2
            if x(isep)<xboundary
                domainG=[domain(1) floor(xboundary)];
                domainD=[floor(xboundary+1) domain(2)];
            else
                domainG=[floor(xboundary+1) domain(2)];
                domainD=[domain(1) floor(xboundary)];
            end
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [isep,heuristic] = sub_BNB_select_var(heuristic,V,x,debug,xstatus)
    isep = 0;
    validVar = find(xstatus==1);
    switch heuristic.choice_Selection
        case 1
            Q = 0;
            if (heuristic.withBi), Q = ceil(size(x,1)/2); end
            xi=x(V-Q); xP=zeros(size(length(validVar),1),1);xP(V-Q)=xi; isep = (find(xP ==max(xi)))+Q;
        case 2
            Q = 0;
            if (heuristic.withBi), Q = ceil(size(x,1)/2); end
            xi=x(V-Q); xP=ones(size(x,1),1); xP(V-Q)=xi; isep = (find(xP ==min(xi) & xP ~= 0))+Q;
        case 3
            if(~isempty(heuristic.ordVarSel)), isep = heuristic.ordVarSel(1); heuristic.ordVarSel(1) = []; else, debug.forceStop = 1; isep = 0; end
    end
end
