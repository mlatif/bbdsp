function [solution] = MIP_BNB_OptL2_CstrL0(H,y,K,solveur,heuristic,debug)
    [N,Q] = size(H);  x0=zeros(Q,1);  params.K = K;
    %
    if (heuristic.withBi)
        params.validBrIdx = [Q+1,2*Q];
        params.xstatus = [zeros(Q,1);ones(Q,1)];
        % [...]
    elseif(~heuristic.withBi)
        x0 = zeros(Q,1);  Aineq = ones(1,Q);  bineq = (1);
        Aeq = []; beq = []; lb = zeros(Q,1);  ub = ones(Q,1);
        params.v0 = x0; params.xstatus= ones(Q,1); params.validBrIdx = [1, Q];
    end
    %
    modele.H = H; modele.y = y; modele.bineq = bineq; modele.Aineq = Aineq;
    modele.Aeq = Aeq; modele.beq = beq; modele.lb = lb; modele.ub = ub; modele.x0 = x0;
    %
    M_OK = 0;
    while(~M_OK)
        [solution] = BNB_OptL2_CstrL0(modele,params,solveur,heuristic,debug);
       M_OK = 1;
    end
end
