%%%%%%%%%%%%%%%%%%%%%%
%%% MAIN
%%%%%%%%%%%%%%%%%%%%%%
clear all   %
close all   %
w = warning ('off','all');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
OS = 3;
mySeed = 'default';
N = 224;
Q = 200;
K = 5;
SNR = 15;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Lancer les deux méthodes
nMethBBD = 18;      % De 1 à 12 et 18 à 19
nMethBBC = 14;      % De 13 à 17
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
sel_heuristic = 1;           % Argmax(x_i)
sep_heuristic = 1;           % brancher bi<-1
form_withBi = 0;             % Sans binaires
withChrono = 1;              % Chronometrages
intTolerance = 1e-8;         % Specifies the amount by which an integer variable can be different from an integer and still be considered feasible.
optimalityTolerance = 1e-9;  % OptimalityTolerance is a tolerance for the first-order optimality measure. If the optimality measure is less than OptimalityTolerance, the iterations end. OptimalityTolerance can also be a relative bound on the first-order optimality measure. See Tolerance Details. First-order optimality measure is defined in First-Order Optimality Measure. (equiv TolFun
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(OS==1)
    addpath('C:\Program Files\IBM\ILOG\CPLEX_Studio126\cplex\matlab\x64_win64');
    addpath('C:\Program Files\IBM\ILOG\CPLEX_Studio126\cplex\examples\src\matlab');
elseif (OS==2)
    %addpath('/opt/ibm/ILOG/CPLEX_Studio126/cplex/matlab');
    %addpath('/opt/ibm/ILOG/CPLEX_Studio126/cplex/examples/src/matlab');
    addpath(genpath('/opt/ibm/ILOG/CPLEX_Studio128/cplex/matlab'));
elseif(OS==3)
    addpath(genpath('/Users/mehdilatif/opt/ibm/ILOG/CPLEX_Studio1210/cplex/matlab'));
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
pandoraBox();
