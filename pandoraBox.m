addpath('./B&B_DEDIE/');
addpath('./B&B_CPLEX/');
typePb = "P20";
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if (nMethBBD ~= 19 || (nMethBBD == 19 && N > Q))
    H = loadUSGSMatrix(N,Q);
    [x_Truth,y] = getInstance(H,N,Q,K,SNR,mySeed);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Branch and bound dedie BNBMethod = 1
    [BNBmethod,preCalcul,restCandidat,solverUsed,dualThreshold,typeBB]=getParam(nMethBBD); typeBB1 = typeBB;
    [solveur,heuristic,debug] = param_Exp(x_Truth,sel_heuristic,sep_heuristic,form_withBi,BNBmethod,withChrono,intTolerance,preCalcul,y,H,restCandidat,solverUsed,dualThreshold,typeBB,optimalityTolerance);
    tic; solution1 = MIP_BNB_OptL2_CstrL0(H,y,K,solveur,heuristic,debug); t1 = toc;
    % Branch and bound CPLEX BNBMethod = 2
    [BNBmethod,preCalcul,restCandidat,solverUsed,dualThreshold,typeBB]=getParam(nMethBBC); typeBB2 = typeBB;
    [solveur,heuristic,debug] = param_Exp(x_Truth,sel_heuristic,sep_heuristic,form_withBi,BNBmethod,withChrono,intTolerance,preCalcul,y,H,restCandidat,solverUsed,dualThreshold,typeBB,optimalityTolerance);
    tic; solution2 = MIP_CPLEX_OptL2_CstrL0(H,y,K,solveur,heuristic,debug); t2 = toc;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Synthese
    x1 = solution1.X;
    x2 = solution2.X;
    fprintf("---\nSynthèse :\n");
    fprintf("Instance : K = %d, Q = %d, SNR = %d\n",K,Q,SNR);
    fprintf("\tMéthode : %s \n",typeBB1);
    fprintf("\t\t||x||_0 = %g, ||x>1e-2||_0 = %g, T1 = %g (sec)\n",sum(x1~=0),sum(x1>1e-2),t1);
    fprintf("\tMéthode : %s \n",typeBB2);
    fprintf("\t\t||x||_0 = %g, ||x>1e-2||_0 = %g, T2 = %g (sec)\n",sum(x2~=0),sum(x2>1e-2),t2);
    fprintf("\tErreurs :\n");
    fprintf("\t\t Données(xt) -  %e\n",norm(y-H*x_Truth)^2);
    fprintf("\t\t %s(x1) :  %e\t delta(x1/xt) = %e \n",typeBB1,norm(y-H*x1)^2, abs(norm(y-H*x_Truth)^2-norm(y-H*x1)^2));
    fprintf("\t\t %s(x2) :  %e\t delta(x2/xt) = %e \n",typeBB2,norm(y-H*x2)^2, abs(norm(y-H*x_Truth)^2-norm(y-H*x2)^2));
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Representation des spectres (observe, reel, calcules) et des composition
    myTitle = strcat('K = ',num2str(K),' Q = ',num2str(Q),' N = ',num2str(N),' SNR = ',num2str(SNR));
    localPlotSpectrum(H,y,x_Truth,x1,x2,myTitle);
    localPlotCompo(x_Truth,x1,x2,myTitle);
    localPlotBoundEvol(solution1,typeBB1)
else
    fprintf("nMethBBD ~= 19 ou N < Q - Approche FCLS non disponible pour les systèmes sous-contraints.\n");
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [H] = loadUSGSMatrix(N,Q)
    load('usgs_library.mat');
    H = spec(1:N,1:Q);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [x_Truth,y] = getInstance(H,N,Q,K,SNR,mySeed)
    rng(mySeed);
    x = zeros(Q,1);
    permP = randperm(Q);
    indNZ = permP(1:K);
    x(indNZ) = rand(K,1);
    x_Truth = x/sum(x);
    y_nonbruite = H*x_Truth;
    sigma_vec = sqrt(norm(y_nonbruite)^2/length(y_nonbruite)*10.^(-SNR/10));
    y = H*x_Truth + sigma_vec*randn(N,1);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [solveur,heuristic,debug] = param_Exp(x_Truth,sel_heuristic,sep_heuristic,form_withBi,BNBmethod,withChrono,intTol,preCalcul,y,H,restCandidat,solverUsed,dualThreshold,BNBname,optTol,Htau)
    debug.ninst = Inf;
    heuristic.BNBmethod = BNBmethod;
    heuristic.solverUsed = solverUsed;
    heuristic.dualThreshold = dualThreshold;
    heuristic.BNBname = BNBname;
    heuristic.K = length(find(x_Truth));
    heuristic.local_dim = [];
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if (heuristic.BNBmethod == 1)
        if (solverUsed==1 || solverUsed ==3)
            solveur = localLoadSolveur(solverUsed,optTol,heuristic.dualThreshold);
        elseif(solverUsed ==2)
            solveur = localLoadSolveur(solverUsed,optTol,heuristic.dualThreshold);
        elseif(solverUsed == 4)
            solveur = localLoadSolveur(solverUsed,optTol,heuristic.dualThreshold);
        elseif(solverUsed == 5 || solverUsed == 6)
            solveur = [];
        end
    elseif(heuristic.BNBmethod ==2)
        solveur = localLoadSolveur(solverUsed,optTol,heuristic.dualThreshold);
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    heuristic.choice_Selection = sel_heuristic;
    heuristic.choice_Separation = sep_heuristic;
    heuristic.ordVarSel=[];
    %heuristic.ordVarSel=[36,4,34,12,5,12,16,1];
    heuristic.nodeStrategy = 'bn';
    heuristic.integTolerance = intTol ;
    heuristic.BBmaxTime = 1000;
    heuristic.withBi = form_withBi;
    if (heuristic.BNBmethod == 2)
        heuristic.withBi = 1;
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if (preCalcul)
        heuristic.preCalcul = 1;
    else
        heuristic.preCalcul = 0;
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if (restCandidat)
        heuristic.resCdd = 1;
    else
        heuristic.resCdd = 0;
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if (solverUsed == 5)
        heuristic.HPtau = 1;    heuristic.HPnbIter = 1000;
    else
        heuristic.HPtau = 0;    heuristic.HPnbIter = 0;
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if (solverUsed == 5)
        heuristic.hybSolver.solverUsed = 1; heuristic.hybSolver.resCdd = 1; heuristic.hybSolver.preCalcul = 1;
        heuristic.hybSolver.solveur = localLoadSolveur(heuristic.hybSolver.solverUsed,optTol,heuristic.dualThreshold);
    else
        heuristic.hybSolver = [];
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if (solverUsed == 6)
        %
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if (heuristic.BNBmethod == 2)
        heuristic.solverUsed = 0; heuristic.choice_Selection = 0; heuristic.choice_Separation = 0;
        heuristic.withBi = 0; heuristic.preCalcul = 0;  heuristic.resCdd = 0;
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    debug.chrono = withChrono;  debug.xtruth = x_Truth; debug.noIterSol = [];
    debug.unitResTime = []; debug.unitObjValue = [];  debug.unitCompSol = [];
    debug.bsup = [];  debug.binf = [];  debug.forceStop = 0;  debug.exitFlag = [];
    debug.traceRouteNodes = []; debug.traceRouteTypeNodes = [];
    debug.traceRouteBoundOps = [];  debug.traceRouteBranchIdx = [];
    debug.traceRouteBinf = [];  debug.traceRouteBsup = [];
    debug.traceRouteGap = [];   debug.traceRouteNoIter = [];
    debug.traceRouteNoIterResol = []; debug.nIter = -1;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    debug.cpt_Bo1 = 0;  debug.cpt_Bo2 = 0;  debug.cpt_Bo3 = 0;    debug.cpt_Br = 0;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    debug.residual = [];  debug.SolveurOutput = []; debug.unitExitFlag = [];
    debug.timeCritReached = 0;  debug.stopFlag = 0;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    debug.lambdaVec = []; debug.detectNegX = [];
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [solveur] = localLoadSolveur(solverUsed,optTol, dualThreshold)
    %disp("SUSED");disp(solverUsed)
    if (solverUsed == 1 || solverUsed == 3)
        solveur = cplexoptimset('cplex');
        solveur.Display = 'off';
        solveur.relaxation=1;
        solveur.MaxTime = 1000;
        solveur.threads = 1;
        solveur.timelimit = 1000;
        solveur.simplex.tolerances.optimality = optTol;
    elseif(solverUsed == 2)
        solveur = optimoptions('quadprog');
        solveur.Display = 'off';
        solveur.MaxIterations = 1000;
        solveur.OptimalityTolerance = optTol;
    elseif(solverUsed == 4)
        solveur = optimoptions('lsqlin');
        solveur.Display = 'off';
        solveur.MaxIterations = 1000;
        solveur.OptimalityTolerance = optTol;
    elseif(solverUsed == 5 || solverUsed == 6)
        solveur = [];
    elseif(solverUsed == 0)
        solveur = cplexoptimset('cplex');
        solveur.Display = 'off';
        solveur.relaxation=1;
        solveur.MaxTime = 1000;
        solveur.threads = 1;
        solveur.mip.tolerances.mipgap = dualThreshold;
        solveur.timelimit = 1000;
        solveur.simplex.tolerances.optimality = optTol;
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [BNBmethod,preCalcul,restCandidat,solverUsed,dualThreshold,BNBname] = getParam(typeMeth)
    BNBname ="";
    switch typeMeth
       case 1, BNBmethod = 1;  preCalcul = 0;  restCandidat = 0; solverUsed = 1; dualThreshold = 0;
       case 2, BNBmethod = 1;  preCalcul = 0;  restCandidat = 0; solverUsed = 2; dualThreshold = 0;
       case 3, BNBmethod = 1;  preCalcul = 0;  restCandidat = 0; solverUsed = 3; dualThreshold = 0;
       case 4, BNBmethod = 1;  preCalcul = 0;  restCandidat = 0; solverUsed = 4; dualThreshold = 0;
       case 5, BNBmethod = 1;  preCalcul = 0;  restCandidat = 1; solverUsed = 1; dualThreshold = 0;
       case 6, BNBmethod = 1;  preCalcul = 0;  restCandidat = 1; solverUsed = 2; dualThreshold = 0;
       case 7, BNBmethod = 1;  preCalcul = 0;  restCandidat = 1; solverUsed = 3; dualThreshold = 0;
       case 8, BNBmethod = 1;  preCalcul = 0;  restCandidat = 1; solverUsed = 4; dualThreshold = 0;
       case 9, BNBmethod = 1;  preCalcul = 1;  restCandidat = 0; solverUsed = 1; dualThreshold = 0;
       case 10, BNBmethod = 1;  preCalcul = 1;  restCandidat = 0; solverUsed = 2; dualThreshold = 0;
       case 11, BNBmethod = 1;  preCalcul = 1;  restCandidat = 1; solverUsed = 1; dualThreshold = 0;
       case 12, BNBmethod = 1;  preCalcul = 1;  restCandidat = 1; solverUsed = 2; dualThreshold = 0;
       case 13, BNBmethod = 2;  preCalcul = 0;  restCandidat = 0; solverUsed = 0; dualThreshold = 1e-04;
       case 14, BNBmethod = 2;  preCalcul = 0;  restCandidat = 0; solverUsed = 0; dualThreshold = 1e-05;
       case 15, BNBmethod = 2;  preCalcul = 0;  restCandidat = 0; solverUsed = 0; dualThreshold = 1e-06;
       case 16, BNBmethod = 2;  preCalcul = 0;  restCandidat = 0; solverUsed = 0; dualThreshold = 1e-07;
       case 17, BNBmethod = 2;  preCalcul = 0;  restCandidat = 0; solverUsed = 0; dualThreshold = 1e-08;
       case 18, BNBmethod = 1;  preCalcul = 1;  restCandidat = 1; solverUsed = 5; dualThreshold = 0;
       case 19, BNBmethod = 1;  preCalcul = 1;  restCandidat = 1; solverUsed = 6; dualThreshold = 0;
    end
    solverName = ["MIP_Cplex","BB_R_Cplx_QP","BB_R_MtLb_QP","BB_R_Cplx_Ls","BB_R_MtLb_Ls","BB_R_Hom","BB_R_FCLS"];
    if (BNBmethod == 1)
        BNBname = num2str(solverName(solverUsed+1));
        if restCandidat, BNBname = strcat(BNBname,'_R');end
        if preCalcul, BNBname = strcat(BNBname,'_PC'); end
    elseif(BNBmethod == 2)
        BNBname = num2str(solverName(1));
        txtDT = num2str(dualThreshold);
        if (dualThreshold == 1e-04)
             BNBname = strcat(BNBname,"_DT-04");
        elseif(dualThreshold ~= 1e-04)
             BNBname = strcat(BNBname,"_DT",erase(txtDT,"1e"));
        end
    end
    fprintf("    Nom : %s \n",BNBname);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [] = localPlotSpectrum(H,y,x_Truth,x1,x2,myTitle)
    myColors = [[0,0.4470,0.7410]; [0.8500 0.3250 0.0980]; [0.9290 0.6940 0.1250];	[0.4940 0.1840 0.5560];		[0.4660 0.6740 0.1880];	[0.3010 0.7450 0.9330];	[0.6350 0.0780 0.1840]];
    figure; hold on ; grid on; box on;
        plot(y,'k','linewidth',1.5);
        plot(H*x_Truth,'-','Color', myColors(1,:),'linewidth',1.5);
        plot(H*x1,'--','linewidth',1.5,'Color', myColors(2,:));
        plot(H*x2,'-.','linewidth',1.5,'Color', myColors(3,:));
        legend("$y$","$Hx_{\textrm{true}}$","$Hx_{\textrm{BBD}}$","$Hx_{\textrm{BBC}}$",'Interpreter','latex');
        title(strcat("Spectre : ",myTitle),'Interpreter','latex');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [] = localPlotCompo(x_Truth,x1,x2,myTitle)
    ind_Truth = find(x_Truth~=0);
    ind1 = find(x1~=0);
    ind2 = find(x2~=0);
    figure; hold on ; grid on; box on;
    stem(ind_Truth,x_Truth(ind_Truth),'r','linewidth',1.5','markersize',8); hold on
    stem(ind1,x1(ind1),'bs--','linewidth',1.5','markersize',8);
    stem(ind2,x2(ind2),'kd--','linewidth',1.5','markersize',8);
    legend('$x_{\textrm{true}}$','$x_{\textrm{BBD}}$','$x_{\textrm{BBC}}$','Interpreter','latex')
    title(strcat("Melange : ",myTitle),'Interpreter','latex')
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [] = localPlotBoundEvol(solution,typeBB1)
    myColors = [[0,0.4470,0.7410]; [0.8500 0.3250 0.0980]; [0.9290 0.6940 0.1250];	[0.4940 0.1840 0.5560];		[0.4660 0.6740 0.1880];	[0.3010 0.7450 0.9330];	[0.6350 0.0780 0.1840]];
    figure;
    hold on
        scatter(solution.noIterSol,solution.unitObjValue,'k','Marker','x')
        plot(solution.traceRoute(:,9),'--','Color',myColors(1,:),'LineWidth',1.4)
        plot(solution.traceRoute(:,8),'--','Color',myColors(4,:),'LineWidth',1.4)
        legend(strcat("$z_{L}^{i}$ $i \in \{1,.,",num2str(solution.nbIter),"\}$"),"$b_{\sup}$","$b_{\inf}$",'Interpreter','latex');
        txt = strcat("Evolution des bornes | ",strrep(typeBB1,"_","\_"));
        title(txt,'Interpreter','latex');
    hold off
end
