# Algorithme branch-and-bound pour le problème de démélange spectral parcimonieux.
## Prototype logiciel développé (en Matlab)
Laboratoire des Sciences du Numérique de Nantes : Equipe [SIMS](https://sims.ls2n.fr/).   
**Stagiaire :** M.Latif [@](mailto:latifm.pro@gmail.com).    
**Encadrants :** S.Bourguignon, R. Ben Mhenni, J. Ninin.   

---


###### Sujet :
Conception d'algorithme branch-and-bound pour le démélange spectral parcimonieux sous contraintes de somme unitaire et de non négativité.   
###### Abstract :
Le problème de démélange spectral vise à déterminer la composition, en termes de matériaux, d’une surface planétaire à partir de la mesure de la réflectance lumineuse sur plusieurs canaux spectraux. Un modèle classique considère que le spectre mesuré pour chaque un pixel peut être expliqué comme un mélange de spectres associés aux composants présents dans la matière et qui peut être exprimé comme une combinaison linéaire de signatures spectrales pures.   
Une contrainte physique, appelée contrainte de parcimonie, stipule que qu’un faible nombre de composants dans le mélange suffit à décrire la composition de chaque spectre mesuré. Par ailleurs, les réflectances mesurées correspondant à une proportion de la lumière éclairant la scène, les coefficients du mélange, représentant des pourcentages, sont positifs et de somme unité.   
Le code présenté est le résultat des travaux de recherche visant à proposer une méthode de résolution exacte pour le problème de démélange parcimonieux intégrant ces contraintes et reposent sur la formulation en nombres mixtes du problème de démélange parcimonieux et de sa résolution par un algorithme de branch-and-bound dédié.   
###### Keywords :
Démélange spectral, Parcimonie, Optimisation exacte, Branch-and-bound.

---

**Contexte :** Master thesis - Master Optimisation en Recherche Opérationnelle - 2020.
